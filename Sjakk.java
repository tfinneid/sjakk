import java.util.*;
import java.util.function.Function;

public class Sjakk {

    public static void main(String []args) {

        Sjakk spill = new Sjakk();
        Sjakkbrett brett = spill.stillOppBrett();
        brett.visBrett();

        spill.kontroll(brett);
    }

    public void kontroll(Sjakkbrett brett) {
        Scanner in = new Scanner(System.in);
        boolean quit = false;
        do {
            System.out.print("> ");
            String cmd = in.next();

            switch (cmd) {
                case "q" -> quit = true;
                case "h" -> System.out.println("q - avslutt\nh - hjelp\n? - vis brett\n?a4 - vis lovlige trekk for a4\nd2-d4 - flytt d2 til d4");
                case "?" -> brett.visBrett();
                default -> avansert(cmd, brett);
            }
        } while (!quit);
    }

    public void avansert(String cmd, Sjakkbrett brett) {
        if ( cmd.startsWith("?") && cmd.length()==3 ) {
            Felt f = new Felt(cmd.substring(1));
            visLovligeTrekk(brett, f);
        } else if( cmd.length()==5 ) {
            Felt fra = new Felt(cmd.substring(0,2));
            Felt til = new Felt(cmd.substring(3));

            if (brett.flyttBrikke(fra, til))
                brett.visBrett();
        }
    }

    public void visLovligeTrekk(Sjakkbrett brett, Felt felt) {
        List<Felt> trekk = brett.finnLovligeTrekk(felt);
        brett.visBrett(trekk);
    }

    public Sjakkbrett stillOppBrett() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hT", "a1")
                .settBrikke("hS", "b1")
                .settBrikke("hL", "c1")
                .settBrikke("hD", "d1")
                .settBrikke("hK", "e1")
                .settBrikke("hL", "f1")
                .settBrikke("hS", "g1")
                .settBrikke("hT", "h1")

                .settBrikke("hB", "a2")
                .settBrikke("hB", "b2")
                .settBrikke("hB", "c2")
                .settBrikke("hB", "d2")
                .settBrikke("hB", "e2")
                .settBrikke("hB", "f2")
                .settBrikke("hB", "g2")
                .settBrikke("hB", "h2")

                .settBrikke("sT", "a8")
                .settBrikke("sS", "b8")
                .settBrikke("sL", "c8")
                .settBrikke("sD", "d8")
                .settBrikke("sK", "e8")
                .settBrikke("sL", "f8")
                .settBrikke("sS", "g8")
                .settBrikke("sT", "h8")

                .settBrikke("sB", "a7")
                .settBrikke("sB", "b7")
                .settBrikke("sB", "c7")
                .settBrikke("sB", "d7")
                .settBrikke("sB", "e7")
                .settBrikke("sB", "f7")
                .settBrikke("sB", "g7")
                .settBrikke("sB", "h7");

        return brett;
    }
}

class Sjakkbrett {

    private Brikke [][]brett = new Brikke[8][8];
    
    private List<Brikke> hviteBrikker = new ArrayList<>();
    private List<Brikke> svarteBrikker = new ArrayList<>();
    
    public void visBrett() {
        visBrett(new ArrayList<>());
    }

    public void visBrett(List<Felt> lovligeTrekk) {

        System.out.println("\n                        Sjakk ruler!!!");
        for (int r=8; r>0; r--) {
            System.out.println("    ---------------------------------------------------------");
            System.out.print(" " + r + "  | ");

            for (int k=1; k<=8; k++) {
                Felt f = new Felt(k,r);
                Brikke brikke = hentBrikke(f);
                String strBrikke;

               if (lovligeTrekk.contains(f) && brikke != null)
                   strBrikke = "*" + brikke.toString() + "*";
               else if (lovligeTrekk.contains(f) && brikke == null)
                   strBrikke = " ** ";
               else if (!lovligeTrekk.contains(f) && brikke != null)
                   strBrikke = " " + brikke.toString() + " ";
               else
                   strBrikke = "    ";

                System.out.print( strBrikke + " | " );
            }
            System.out.println();
        }

        System.out.println("    ---------------------------------------------------------");
        System.out.println("       a      b      c      d      e      f      g      h");
        System.out.println();

        System.out.print("Felte svarte brikker: ");
        svarteBrikker.forEach( b -> System.out.print(b + ", ") );
        System.out.print("\nFelte hvite brikker: ");
        hviteBrikker.forEach( b -> System.out.print(b + ", ") );
        System.out.println();
    }

    public Sjakkbrett settBrikke(String brikkenavn, String feltnavn) {
        Felt felt = new Felt(feltnavn);
        Brikke brikke = Brikke.navn(brikkenavn);
        return settBrikke(brikke, felt);
    }

    public Sjakkbrett settBrikke(Brikke brikke, Felt felt) {
        brett[felt.rad-1][felt.kolonne-1] = brikke;
        return this;
    }

    public Brikke hentBrikke(Felt felt) {
        return brett[felt.rad-1][felt.kolonne-1];
    }

    public void fjernBrikke(Felt felt) {
        brett[felt.rad-1][felt.kolonne-1] = null;
    }

    public boolean flyttBrikke(Felt fra, Felt til) {
        Brikke fraBrikke = hentBrikke(fra);
        List<Felt> lovligeTrekk = finnLovligeTrekk(fra);
        
        if (!lovligeTrekk.contains(til)) {
            System.out.println("!!Ulovlig trekk. Kun følgende er lovlig");
            visBrett(lovligeTrekk);
            return false;
        }
            
        fjernBrikke(fra);

        Brikke tilBrikke = hentBrikke(til);
        if (tilBrikke != null && tilBrikke.farge == Farge.h) {
            hviteBrikker.add(tilBrikke);
        } else if (tilBrikke != null) {
            svarteBrikker.add(tilBrikke);
        }
                
        settBrikke(fraBrikke, til);
        return true;
    }

    public List<Felt> finnLovligeTrekk(Felt felt) {
        Brikke brikke = hentBrikke(felt);
        if (brikke == null) return new ArrayList<>();

        return brikke.finnLovligeTrekk(this, felt);
    }
}

class Felt {
    int kolonne;
    int rad;

    public Felt(String feltnavn) {
        this(feltnavn.charAt(0) - 96,
             feltnavn.charAt(1) - 48);
    }

    Felt(int kolonne, int rad) {
        this.kolonne = kolonne;
        this.rad = rad;

        Verification.verify(gyldigFelt(kolonne));
        Verification.verify(gyldigFelt(rad));
    }

    boolean gyldigFelt(int felttall) {
        return gyldigFelt(felttall, 8);
    }

    boolean gyldigFelt(int felttall, int rekkevidde) {
        return felttall >= 1 && felttall <= 8 &&
               (felttall <= rad+rekkevidde && felttall >= rad-rekkevidde &&
                felttall <= kolonne+rekkevidde && felttall >= kolonne-rekkevidde);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Felt felt = (Felt) o;
        return kolonne == felt.kolonne &&
                rad == felt.rad;
    }

    @Override
    public int hashCode() {
        return Objects.hash(kolonne, rad);
    }

    public String toString() {
        return "" + ((char) (kolonne + 96)) + rad;
    }
}

abstract class Brikke {

    protected Farge farge;

    static Map<Character, Function<Farge,? extends Brikke>> typenavn = Map.of(
            'B', Bonde::new,
            'T', Tårn::new,
            'S', Springer::new,
            'L', Løper::new,
            'D', Dronning::new,
            'K', Konge::new);

    public Brikke(Farge farge) {
        this.farge = farge;
    }

    public abstract List<Felt> finnLovligeTrekk(Sjakkbrett brett, Felt felt);

    static Brikke navn(String brikkenavn) {
        if (brikkenavn.length() != 2)
            throw new RuntimeException("Ugyldig brikkenavn, burde være 'hD', 'sB',..., men fikk: " + brikkenavn);

        Farge fargenavn = Farge.navn(brikkenavn.charAt(0));
        char t = brikkenavn.charAt(1);
        
        Brikke brikketype = typenavn.get( t ).apply(fargenavn);
        if (brikketype == null) throw new RuntimeException("Type brikke eksisterer ikke: " + t);
        brikketype.farge = fargenavn;

        return brikketype;
    }

    public String toString() {
        return farge+""+toString();
    }
}

enum Farge {
    s, h;

    static Farge navn(char n) {
        return switch (n) {
            case 'h' -> Farge.h;
            case 's' -> Farge.s;
            default -> throw new RuntimeException("kun hvit eller svart");
        };
    }
}

class Bonde extends Brikke {

    public Bonde(Farge farge) {
        super(farge);
    }

    public List<Felt> finnLovligeTrekk(Sjakkbrett brett, Felt felt) {
        Brikke brikke = brett.hentBrikke(felt);

        //System.out.println("Fant brikke: " + brikke + "(rad: "+felt.rad+" kolonne: "+felt.kolonne+")");

        List<Felt> trekk = new ArrayList<>();
        int max;

        if (brikke.farge == Farge.h)
            max = felt.rad == 2 ? 2 : 1;
        else
            max = felt.rad == 7 ? 2 : 1;

//        int max = (felt.rad==7||felt.rad==2) ? 2 : 1;

        Brikke nbrikke = null;
        if (brikke.farge == Farge.h) {
            for (int r = felt.rad + 1; felt.gyldigFelt(r) && r <= felt.rad+max && nbrikke == null; r++) {
                Felt nfelt = new Felt(felt.kolonne, r);
                nbrikke = brett.hentBrikke(nfelt);
                if (nbrikke == null)
                    trekk.add(nfelt);
            }

            Felt nfelt = new Felt(felt.kolonne+1, felt.rad+1);
            nbrikke = brett.hentBrikke(nfelt);
            if (nbrikke != null && nbrikke.farge == Farge.s)
                trekk.add(nfelt);

            int nk = felt.kolonne+1, nr = felt.rad-1;
            if (felt.gyldigFelt(nk) && felt.gyldigFelt(nr)) {
                nfelt = new Felt(felt.kolonne - 1, felt.rad + 1);
                nbrikke = brett.hentBrikke(nfelt);
                if (nbrikke != null && nbrikke.farge == Farge.s)
                    trekk.add(nfelt);
            }
        } else {
            nbrikke = null;
            for (int r = felt.rad - 1; felt.gyldigFelt(r) && r >= felt.rad-max  && nbrikke == null; r--) {
                Felt nfelt = new Felt(felt.kolonne, r);
                nbrikke = brett.hentBrikke(nfelt);
                if (nbrikke == null)
                    trekk.add(nfelt);
            }

            int nk = felt.kolonne+1, nr = felt.rad-1;
            if (felt.gyldigFelt(nk) && felt.gyldigFelt(nr)) {
                Felt nfelt = new Felt(nk,nr);
                nbrikke = brett.hentBrikke(nfelt);
                if (nbrikke != null && nbrikke.farge == Farge.h)
                    trekk.add(nfelt);
            }

            nk = felt.kolonne-1; nr = felt.rad-1;
            if (felt.gyldigFelt(nk) && felt.gyldigFelt(nr)) {
                Felt nfelt = new Felt(nk, nr);
                nbrikke = brett.hentBrikke(nfelt);
                if (nbrikke != null && nbrikke.farge == Farge.h)
                    trekk.add(nfelt);
            }
        }

        return trekk;
    }

    public String toString() {
        return farge+"B";
    }
}

class Tårn extends Brikke {

    public Tårn(Farge farge) {
        super(farge);
    }

    public List<Felt> finnLovligeTrekk(Sjakkbrett brett, Felt felt) {
        Brikke brikke = brett.hentBrikke(felt);

        //System.out.println("Fant brikke: " + brikke + "(rad: "+felt.rad+" kolonne: "+felt.kolonne+")");

        List<Felt> trekk = new ArrayList<>();

        Brikke nbrikke = null;
        for (int r = felt.rad + 1; felt.gyldigFelt(r) && nbrikke == null; r++) {
            Felt nfelt = new Felt(felt.kolonne, r);
            nbrikke = brett.hentBrikke(nfelt);
            if (nbrikke == null || nbrikke.farge != brikke.farge)
                trekk.add(nfelt);
        }
        nbrikke = null;
        for (int r = felt.rad - 1; felt.gyldigFelt(r) && nbrikke == null; r--) {
            Felt nfelt = new Felt(felt.kolonne, r);
            nbrikke = brett.hentBrikke(nfelt);
            if (nbrikke == null || nbrikke.farge != brikke.farge)
                trekk.add(nfelt);
        }
        nbrikke = null;
        for (int k = felt.kolonne + 1; felt.gyldigFelt(k) && nbrikke == null; k++) {
            Felt nfelt = new Felt(k, felt.rad);
            nbrikke = brett.hentBrikke(nfelt);
            if (nbrikke == null || nbrikke.farge != brikke.farge)
                trekk.add(nfelt);
        }
        nbrikke = null;
        for (int k = felt.kolonne - 1; felt.gyldigFelt(k) && nbrikke == null; k--) {
            Felt nfelt = new Felt(k, felt.rad);
            nbrikke = brett.hentBrikke(nfelt);
            if (nbrikke == null || nbrikke.farge != brikke.farge)
                trekk.add(nfelt);
        }
        
        return trekk;
    }

    public String toString() {
        return farge+"T";
    }
}
class Springer extends Brikke  {

    public Springer(Farge farge) {
        super(farge);
    }

    public List<Felt> finnLovligeTrekk(Sjakkbrett brett, Felt felt) {
        Brikke brikke = brett.hentBrikke(felt);

        //System.out.println("Fant brikke: " + brikke + "(rad: " + felt.rad + " kolonne: " + felt.kolonne + ")");

        List<Felt> trekk = new ArrayList<>();
        return trekk;
    }

    public String toString() {
        return farge+"S";
    }
}
class Løper extends Brikke  {

    public Løper(Farge farge) {
        super(farge);
    }

    public List<Felt> finnLovligeTrekk(Sjakkbrett brett, Felt felt) {
        Brikke brikke = brett.hentBrikke(felt);

        //System.out.println("Fant brikke: " + brikke + "(rad: "+felt.rad+" kolonne: "+felt.kolonne+")");

        List<Felt> trekk = new ArrayList<>();
    
        Brikke nbrikke = null;
        for (int r=felt.rad+1, k=felt.kolonne+1; felt.gyldigFelt(r) && felt.gyldigFelt(k) && nbrikke == null; r++, k++) {
            Felt nfelt = new Felt(k,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }
        
        nbrikke = null;
        for (int r=felt.rad-1, k=felt.kolonne-1; felt.gyldigFelt(r) && felt.gyldigFelt(k) && nbrikke == null; r--, k--) {
            Felt nfelt = new Felt(k,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }
        
        nbrikke = null;
        for (int r=felt.rad+1, k=felt.kolonne-1; felt.gyldigFelt(r) && felt.gyldigFelt(k) && nbrikke == null; r++, k--) {
            Felt nfelt = new Felt(k,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }
        
        nbrikke = null;
        for (int r=felt.rad-1, k=felt.kolonne+1; felt.gyldigFelt(r) && felt.gyldigFelt(k) && nbrikke == null; r--, k++) {
            Felt nfelt = new Felt(k,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }
        return trekk;
    }

    public String toString() {
        return farge+"L";
    }
}
class Dronning extends Brikke {

    public Dronning(Farge farge) {
        super(farge);
    }

    public List<Felt> finnLovligeTrekk(Sjakkbrett brett, Felt felt) {
        Brikke brikke = brett.hentBrikke(felt);

        //System.out.println("Fant brikke: " + brikke + "(rad: " + felt.rad + " kolonne: " + felt.kolonne + ")");

        List<Felt> trekk = new ArrayList<>();
        return trekk;
    }

    public String toString() {
        return farge+"D";
    }
}
class Konge extends Brikke {

    public Konge(Farge farge) {
        super(farge);
    }

    public List<Felt> finnLovligeTrekk(Sjakkbrett brett, Felt felt) {

        Brikke brikke = brett.hentBrikke(felt);

        //System.out.println("Fant brikke: " + brikke + "(rad: "+felt.rad+" kolonne: "+felt.kolonne+")");

        List<Felt> trekk = new ArrayList<>();
        int rekkevidde = 1;
        
        // Vertikal/horisontal
        
        Brikke nbrikke = null;
        for (int r=felt.rad+1; felt.gyldigFelt(r,rekkevidde) && nbrikke == null; r++) {
            Felt nfelt = new Felt(felt.kolonne,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }
        nbrikke = null;
        for (int r=felt.rad-1; felt.gyldigFelt(r,rekkevidde) && nbrikke == null; r--) {
            Felt nfelt = new Felt(felt.kolonne,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }
        nbrikke = null;
        for (int k=felt.kolonne+1; felt.gyldigFelt(k,rekkevidde) && nbrikke == null; k++) {
            Felt nfelt = new Felt(k,felt.rad);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }
        nbrikke = null;
        for (int k=felt.kolonne-1; felt.gyldigFelt(k,rekkevidde) && nbrikke == null; k--) {
            Felt nfelt = new Felt(k,felt.rad);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }

        // Diagonal

        nbrikke = null;
        for (int r=felt.rad+1, k=felt.kolonne+1; felt.gyldigFelt(r,rekkevidde) && felt.gyldigFelt(k,rekkevidde) && nbrikke == null; r++, k++) {
            Felt nfelt = new Felt(k,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }

        nbrikke = null;
        for (int r=felt.rad-1, k=felt.kolonne-1; felt.gyldigFelt(r,rekkevidde) && felt.gyldigFelt(k,rekkevidde) && nbrikke == null; r--, k--) {
            Felt nfelt = new Felt(k,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }

        nbrikke = null;
        for (int r=felt.rad+1, k=felt.kolonne-1; felt.gyldigFelt(r,rekkevidde) && felt.gyldigFelt(k,rekkevidde) && nbrikke == null; r++, k--) {
            Felt nfelt = new Felt(k,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }

        nbrikke = null;
        for (int r=felt.rad-1, k=felt.kolonne+1; felt.gyldigFelt(r,rekkevidde) && felt.gyldigFelt(k,rekkevidde) && nbrikke == null; r--, k++) {
            Felt nfelt = new Felt(k,r);
            nbrikke = brett.hentBrikke(nfelt);
	    if (nbrikke == null || nbrikke.farge != brikke.farge) 
		trekk.add(nfelt);
        }
        
        
        return trekk;
    }

    public String toString() {
        return farge+"K";
    }
}
