import java.util.List;

public class Tester {

    public static void main(String[] args) {
        Tester t = new Tester();
        t.kjørTester();
    }

    void assertTrue(boolean expr, String msg) {
        if (!expr)
            throw new RuntimeException("  \"" + msg + "\" : [Forventet verdi: \""+expr+"\"]");
    }

    void assertTrue(boolean expr) {
        assertTrue(expr, "[INGEN]");
    }

    public void kjørTester() {

        testFelter();
        test99();

        testBondeA2();
        testBondeA4();
        testBondeC5();
        testTårnA3();
        testTårnF2();
        testTårnF6();
        testLøperD5();
        testLøperE5();
        testLøperG3();
        testLøperA2();
        testKongeD4();
        testKongeF6();
        testKongeE1();
    }

    void testBondeA2() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hB", "a2")
                .settBrikke("hT", "b4");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "a2");
        assertTrue(trekk.size() == 2);
        assertTrue(trekk.get(0).toString().toString().equals("a3"));
        assertTrue(trekk.get(1).toString().equals("a4"));
    }

    void testBondeA4() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hB", "a2")
                .settBrikke("hT", "a4");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "a2");
        assertTrue(trekk.size() == 1);
        assertTrue(trekk.get(0).toString().equals("a3"));
    }

    void testBondeC5() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hB", "c5")
                .settBrikke("hT", "b4");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "c5");
        assertTrue(trekk.size() == 1);
        assertTrue(trekk.get(0).toString().equals("c6"));
    }

    void testTårnA3() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hB", "a2")
                .settBrikke("hT", "a3");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "a2");
        assertTrue(trekk.size() == 0);
    }

    void testTårnF2() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hT", "f2");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "f2");
        assertTrue(trekk.size() == 14);
        assertTrue(trekk.get(0).toString().equals("f3"));
        assertTrue(trekk.get(1).toString().equals("f4"));
        assertTrue(trekk.get(2).toString().equals("f5"));
        assertTrue(trekk.get(3).toString().equals("f6"));
        assertTrue(trekk.get(4).toString().equals("f7"));
        assertTrue(trekk.get(5).toString().equals("f8"));
        assertTrue(trekk.get(6).toString().equals("f1"));
        assertTrue(trekk.get(7).toString().equals("g2"));
        assertTrue(trekk.get(8).toString().equals("h2"));
        assertTrue(trekk.get(9).toString().equals("e2"));
        assertTrue(trekk.get(10).toString().equals("d2"));
        assertTrue(trekk.get(11).toString().equals("c2"));
        assertTrue(trekk.get(12).toString().equals("b2"));
        assertTrue(trekk.get(13).toString().equals("a2"));
    }

    void testTårnF6() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hT", "f2")
                .settBrikke("hT", "f6");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "f2");
        assertTrue(trekk.size() == 11);
        assertTrue(trekk.get(0).toString().equals("f3"));
        assertTrue(trekk.get(1).toString().equals("f4"));
        assertTrue(trekk.get(2).toString().equals("f5"));
        assertTrue(trekk.get(3).toString().equals("f1"));
        assertTrue(trekk.get(4).toString().equals("g2"));
        assertTrue(trekk.get(5).toString().equals("h2"));
        assertTrue(trekk.get(6).toString().equals("e2"));
        assertTrue(trekk.get(7).toString().equals("d2"));
        assertTrue(trekk.get(8).toString().equals("c2"));
        assertTrue(trekk.get(9).toString().equals("b2"));
        assertTrue(trekk.get(10).toString().equals("a2"));
    }

    void testTårnC2() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hT", "f2")
                .settBrikke("hT", "c2");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "f2");
        assertTrue(trekk.size() == 12);
        assertTrue(trekk.get(0).toString().equals("f3"));
        assertTrue(trekk.get(1).toString().equals("f4"));
        assertTrue(trekk.get(2).toString().equals("f5"));
        assertTrue(trekk.get(3).toString().equals("f6"));
        assertTrue(trekk.get(4).toString().equals("f1"));
    }

    void testLøperD5() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hL", "d5")
                .settBrikke("hB", "b7")
                .settBrikke("hB", "a2")
                .settBrikke("hB", "g2")
                .settBrikke("hB", "f7");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "d5");
    }

    void testLøperE5() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hL", "e5");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "e5");
    }

    void testLøperG3() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hL", "g3");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "g3");
    }

    void testLøperA2() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hL", "a2");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "a2");
        
        brett = new Sjakkbrett().settBrikke("hL", "h5");
        sjekkLovligeTrekk(brett, "h5");
        brett = new Sjakkbrett().settBrikke("hL", "h7");
        sjekkLovligeTrekk(brett, "h7");
        brett = new Sjakkbrett().settBrikke("hL", "a3");
        sjekkLovligeTrekk(brett, "a3");
        brett = new Sjakkbrett().settBrikke("hL", "a7");
        sjekkLovligeTrekk(brett, "a7");
        brett = new Sjakkbrett().settBrikke("hL", "d8");
        sjekkLovligeTrekk(brett, "d8");
        brett = new Sjakkbrett().settBrikke("hL", "f6");
        sjekkLovligeTrekk(brett, "f6");
        brett = new Sjakkbrett().settBrikke("hL", "d5");
        sjekkLovligeTrekk(brett, "d5");
    }

    void testKongeD4() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hK", "d4");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "d4");
    }

    void testKongeF6() {
        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hK", "f6")
                .settBrikke("sL", "g7");

        List<Felt> trekk = sjekkLovligeTrekk(brett, "f6");
    }

    void testKongeE1() {
        Sjakkbrett brett = new Sjakkbrett().settBrikke("hK", "f7");
        sjekkLovligeTrekk(brett, "f7");
        
        brett = new Sjakkbrett().settBrikke("hK", "f5");
        sjekkLovligeTrekk(brett, "f5");
    }


    List<Felt> sjekkLovligeTrekk(Sjakkbrett brett, String felt) {
        System.out.println("\nViser hele brettet for " + felt);

        Felt f = new Felt(felt);
        List<Felt> trekk = brett.finnLovligeTrekk(f);
        brett.visBrett(trekk);
        System.out.print("Lovlige trekk: ");
        trekk.forEach( s -> System.out.print(s.toString() + ", "));
        System.out.println();
        return trekk;

    }

    void test99() {
        System.out.println("\nViser hele brettet med fullt oppsett.");

        Sjakkbrett brett = new Sjakkbrett()
                .settBrikke("hT", "a1")
                .settBrikke("hS", "b1")
                .settBrikke("hL", "c1")
                .settBrikke("hD", "d1")
                .settBrikke("hK", "e1")
                .settBrikke("hL", "f1")
                .settBrikke("hS", "g1")
                .settBrikke("hT", "h1")

                .settBrikke("hB", "a2")
                .settBrikke("hB", "b2")
                .settBrikke("hB", "c2")
                .settBrikke("hB", "d2")
                .settBrikke("hB", "e2")
                .settBrikke("hB", "f2")
                .settBrikke("hB", "g2")
                .settBrikke("hB", "h2")

                .settBrikke("sT", "a8")
                .settBrikke("sS", "b8")
                .settBrikke("sL", "c8")
                .settBrikke("sD", "d8")
                .settBrikke("sK", "e8")
                .settBrikke("sL", "f8")
                .settBrikke("sS", "g8")
                .settBrikke("sT", "h8")

                .settBrikke("sB", "a7")
                .settBrikke("sB", "b7")
                .settBrikke("sB", "c7")
                .settBrikke("sB", "d7")
                .settBrikke("sB", "e7")
                .settBrikke("sB", "f7")
                .settBrikke("sB", "g7")
                .settBrikke("sB", "h7");


        brett.visBrett();
    }

    void testFelter() {
        testFelt("c1",3,1);
        testFelt("g7",7,7);
        testFelt("a4",1,4);
        testFelt("e2",5,2);
        testFelt("a1",1,1);
        testFelt("h8",8,8);
    }

    void testFelt(String fn, int kolonne, int rad) {
        Felt f = new Felt(fn);
        assertTrue(f.rad == rad);
        assertTrue(f.kolonne == kolonne);
        System.out.println("Felt: " + fn + " = rad: "+ f.rad +"  kolonne: " + f.kolonne);
    }
}


class Verification {

    static void verify(boolean expr) {
        if (!expr)
            throw new RuntimeException("Verifisering feilet");
    }

}

    
